# frozen_string_literal: true

require_relative '../lib/devops_labels'

Gitlab::Triage::Resource::Context.include DevopsLabels::Context
