# frozen_string_literal: true

require 'spec_helper'
require 'gitlab/triage/engine'
require_relative '../../lib/bug_schedule_helper'
require_relative '../../lib/slo_breach_helper'
require_relative '../../lib/team_member_select_helper'
require_relative '../../lib/www_gitlab_com'

RSpec.describe BugScheduleHelper do
  let(:bug_schedule_helper) do
    Class.new do
      attr_reader :milestone

      include BugScheduleHelper
      include SloBreachHelper
      include TeamMemberSelectHelper

      def initialize(milestone)
        @milestone = milestone
      end

      def current_group_label
        'group::triageops'
      end

      def current_group_name_with_spaces
        'triageops'
      end
    end
  end

  let(:today) { '2022-07-07' }
  let(:five_days_ago) { '2022-07-02' }
  let(:five_days_later) { '2022-07-12' }

  let(:milestone) do
    Gitlab::Triage::Resource::Milestone.new({ title: "15.3", due_date: five_days_later, start_date: five_days_ago })
  end

  let(:developer) do
    { "role" => "backend engineer",
      "departments" => ["Engineering Function", "Development Department"],
      "specialty" => nil }
  end

  subject { bug_schedule_helper.new(milestone) }

  around do |example|
    Timecop.freeze(today) { example.run }
  end

  describe '#milestone_matches_slo_target?' do
    let(:one_year_ago) { "2021-01-10" }
    let(:one_year_later) { "2023-07-01" }

    it 'returns false when issue does not belong to any milestone' do
      expect(subject.milestone_matches_slo_target?(nil, "severity::1")).to eq(false)
    end

    context 'when milestone is expired' do
      let(:expired_milestone) do
        Gitlab::Triage::Resource::Milestone.new({ due_date: one_year_ago, start_date: one_year_ago })
      end

      it 'returns false' do
        expect(subject.milestone_matches_slo_target?(expired_milestone, "severity::1")).to eq(false)
      end
    end

    context 'when milestone start date is later than the slo target date' do
      let(:milestone_too_far) do
        Gitlab::Triage::Resource::Milestone.new({ due_date: one_year_later, start_date: one_year_later })
      end

      it 'returns false' do
        expect(subject.milestone_matches_slo_target?(milestone_too_far, "severity::1")).to eq(false)
      end
    end

    context 'when milestone is not expired and has a start date earlier than slo target date' do
      it 'returns true' do
        expect(subject.milestone_matches_slo_target?(milestone, "severity::1")).to eq(true)
      end
    end
  end

  describe '#em_for_current_group' do
    context "when frontend em is found for the current group" do
      before do
        allow(subject).to receive(:frontend_em_for_team).and_return("@fe_em")
      end

      it 'returns the frontend em username' do
        expect(subject.em_for_current_group).to eq("@fe_em")
      end
    end

    context "when frontend em is not found, but backend em is found for the current group" do
      before do
        allow(subject).to receive(:frontend_em_for_team).and_return(nil)
        allow(subject).to receive(:backend_em_for_team).and_return("@be_em")
      end

      it 'returns the backend em username' do
        expect(subject.em_for_current_group).to eq("@be_em")
      end
    end
  end

  describe '#pm_for_current_group' do
    context "when product manager is found for the current group" do
      before do
        allow(subject).to receive(:pm_for_team).and_return("@pm")
      end

      it 'returns the product manager username' do
        expect(subject.pm_for_current_group).to eq("@pm")
      end
    end

    context "when product manager is not found for the current group" do
      before do
        allow(subject).to receive(:pm_for_team).and_return(nil)
      end

      it 'returns nil' do
        expect(subject.pm_for_current_group).to eq(nil)
      end
    end
  end

  describe '#set_for_current_group' do
    let(:set) do
      { "role" => "Senior Software Engineer in Test",
        "departments" => ["Engineering Function", "Quality Department"] }
    end

    before do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_data)
    end

    context "when SETs populated their team info in the specialty field" do
      let(:team_data) do
        { "dev0" => developer,
          "set1" => set.merge({ "specialty" => "triageops" }),
          "set2" => set.merge({ "specialty" => "triageops" }) }
      end

      it "returns the username of the first SET" do
        expect(subject.set_for_current_group).to eq("@set1")
      end
    end

    context "when SETs populated team info in the role field" do
      let(:team_data) do
        { "dev0" => developer,
          "set1" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, triageops" }),
          "set2" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, triageops" }) }
      end

      it "returns the username of the first SET" do
        expect(subject.set_for_current_group).to eq("@set1")
      end
    end

    context "when multiple SETs found, some populated group info in specialty field, some in the role field" do
      let(:team_data) do
        { "dev0" => developer,
          "set1" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, triageops" }),
          "set2" => set.merge({ "specialty" => "triageops" }) }
      end

      it "returns the username of the SET who specified team info in specialty" do
        expect(subject.set_for_current_group).to eq("@set2")
      end
    end

    context "when SET is not found for the current group" do
      let(:team_data) do
        { "dev0" => developer,
          "dev1" => developer }
      end

      it 'returns nil' do
        expect(subject.set_for_current_group).to eq(nil)
      end
    end
  end

  describe '#pd_for_current_group' do
    let(:designer) do
      { "role" => "Product Designer",
        "departments" => ["Engineering Function", "UX Department"] }
    end

    before do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_data)
    end

    context "when product designers populated team info in the specialty field" do
      let(:team_data) do
        { "dev0" => developer,
          "pd1" => designer.merge({ "specialty" => "triageops" }) }
      end

      it "returns the username of the first product designer" do
        expect(subject.pd_for_current_group).to eq("@pd1")
      end
    end

    context "when product designers populated team info in the role field" do
      let(:team_data) do
        { "dev0" => developer,
          "pd1" => designer.merge({ "specialty" => nil,  "role" => "Product Designer, triageops" }),
          "pd2" => designer.merge({ "specialty" => nil,  "role" => "Product Designer, triageops" }) }
      end

      it "returns the username of the first product designer" do
        expect(subject.pd_for_current_group).to eq("@pd1")
      end
    end

    context "when some product designers populated team info in the role field, some in special field" do
      let(:team_data) do
        { "dev0" => developer,
          "pd1" => designer.merge({ "specialty" => nil,  "role" => "Product Designer, triageops" }),
          "pd2" => designer.merge({ "specialty" => "triageops" }) }
      end

      it "returns the username of product designer who specified team info in specialty" do
        expect(subject.pd_for_current_group).to eq("@pd2")
      end
    end

    context "when product designer is not found for the current group" do
      let(:team_data) do
        { "dev0" => developer,
          "dev1" => developer }
      end

      it 'returns nil' do
        expect(subject.pd_for_current_group).to eq(nil)
      end
    end
  end

  describe '#mention_team_members_for_slo_target_mismatch' do
    context "when none of the team members is found" do
      before do
        allow(subject).to receive(:em_for_current_group).and_return(nil)
        allow(subject).to receive(:pm_for_current_group).and_return(nil)
        allow(subject).to receive(:set_for_current_group).and_return(nil)
        allow(subject).to receive(:pd_for_current_group).and_return(nil)
      end

      it "returns the group label instead" do
        expect(subject.mention_team_members_for_slo_target_mismatch).to eq(%(~"group::triageops"))
      end
    end

    context "when some of the team members are found" do
      before do
        allow(subject).to receive(:em_for_current_group).and_return('@em')
        allow(subject).to receive(:pm_for_current_group).and_return(nil)
        allow(subject).to receive(:set_for_current_group).and_return('@set')
        allow(subject).to receive(:pd_for_current_group).and_return('@pd')
      end

      it "returns string interpolted with every available username" do
        expect(subject.mention_team_members_for_slo_target_mismatch).to eq('@em @set @pd')
      end
    end
  end

  describe '#set_milestone_with_slo_target' do
    let(:milestone_valid_for_slo) do
      Gitlab::Triage::Resource::Milestone.new({ title: '17.0' })
    end

    before do
      allow_any_instance_of(VersionedMilestone).to receive(:find_milestone_for_date).with(Date.parse(today) + 30).and_return(milestone_valid_for_slo)
    end

    it 'returns milestone matching the given date' do
      expect(subject.set_milestone_with_slo_target("severity::1")).to eq(%(/milestone %"17.0"))
    end
  end
end
