# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  class BreakingChangeComment < Processor
    BREAKING_CHANGE_LABEL = 'breaking change'.freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_gitlab_org? &&
        breaking_change_label_added? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(breaking_change_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor is triggered when a merge request is labeled with `~breaking change`.
        It provides link to GitLab docs regarding breaking changes, and suggests ways to assess the cause and impact.
      TEXT
    end

    private

    def breaking_change_label_added?
      event.added_label_names.include?(BREAKING_CHANGE_LABEL)
    end

    def breaking_change_comment
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username} thanks for adding the ~"#{BREAKING_CHANGE_LABEL}" label!

        This merge request introduces breaking changes. [Learn more about breaking changes](https://docs.gitlab.com/ee/development/contributing/index.html#breaking-changes).

        It's important to identify how the breaking change was introduced. To estimate the impact, try to assess the following:

        - Are there existing users depending on this feature?
          - Are self-managed customers affected?
          - To verify and quantify usage, use Grafana or Kibana.
          - If you're not sure about how to query the data, contact the infrastructure team on their Slack channel, #infrastructure-lounge
        - Was sufficient time given to communicate the change?
        - Changes in the permissions, the API schema, and the API response might affect existing 3rd party integrations.
          - Reach out to the Support team or Technical Account Managers and ask about the possible impact of this change.
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
