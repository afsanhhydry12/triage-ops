# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RequireTypeOnRefinement < Processor
    READY_FOR_DEV_LABEL = 'workflow::ready for development'
    REFINEMENT_LABEL = 'workflow::refinement'

    react_to 'issue.update'

    def applicable?
      event.from_part_of_product_project? &&
        ready_for_development_added? &&
        !event.type_label_set?
    end

    def process
      send_to_refinement
    end

    def documentation
      <<~TEXT
        This processor send an issue back when it progresses to workflow::ready for development but doesn't have a type::.
        Setting a type:: label is part of the refinement steps for engineering.
        https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/994#note_940356092
      TEXT
    end

    private

    def ready_for_development_added?
      event.added_label_names.include?(READY_FOR_DEV_LABEL)
    end

    def send_to_refinement
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username}, thanks for adding the ~"#{READY_FOR_DEV_LABEL}" label.

        This issue has been sent back to ~"#{REFINEMENT_LABEL}" because it's missing the
        [work type classification](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification).

        Please add a `type::` label and try again.

        /label ~"#{REFINEMENT_LABEL}"
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
