# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class BackstageLabel < Processor
    BACKSTAGE_LABELS = [
      'backstage',
      'backstage [DEPRECATED]'
    ].freeze

    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        backstage_label_added?
    end

    def process
      post_deprecation_message
    end

    def documentation
      <<~TEXT
        This processor hints and removes `~"backstage [DEPRECATED]"` label because we don't want to use this label anymore.
      TEXT
    end

    private

    def backstage_label_added?
      !!backstage_label_added
    end

    def backstage_label_added
      (event.added_label_names & BACKSTAGE_LABELS).first
    end

    def post_deprecation_message
      comment = <<~MARKDOWN.chomp
        Hey @#{event.event_actor_username}, ~"#{backstage_label_added}" is being deprecated in favor of ~"type::feature", ~"feature::addition", ~"type::maintenance", ~"maintenance::pipelines", and ~"maintenance::workflow" to improve the identification of these type of changes.
        Please see https://about.gitlab.com/handbook/engineering/metrics/#data-classification for further guidance.
        /unlabel ~"#{backstage_label_added}"
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
