# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class ResetReviewState < CommunityProcessor
    AUTOMATION_REVIEWERS_REMINDED_LABEL = 'automation:reviewers-reminded'

    react_to 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        workflow_in_dev_added?
    end

    def process
      reset_review_state
    end

    def documentation
      <<~TEXT
        This processor removes the ~"automation:reviewers-reminded" label from community merge requests when the ~"workflow::in dev" label is added.
      TEXT
    end

    private

    def reset_review_state
      add_comment(%(/unlabel ~"#{AUTOMATION_REVIEWERS_REMINDED_LABEL}"), append_source_link: false)
    end
  end
end
