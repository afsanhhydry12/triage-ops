# frozen_string_literal: true

require_relative 'community_processor'
require_relative 'automated_review_request_generic'
require_relative '../../triage/documentation_code_owner'
require_relative '../../triage/changes_checker'

module Triage
  class AutomatedReviewRequestDoc < CommunityProcessor
    DOC_FILE_REGEX = %r{\Adocs?/}.freeze
    DOCUMENTATION_LABEL = 'documentation'
    TECHNICAL_WRITING_LABEL = 'Technical Writing'
    TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'
    GL_DOCSTEAM_HANDLE = 'gl-docsteam'
    TECHNICAL_WRITER_GROUPS = [GL_DOCSTEAM_HANDLE, 'gitlab-com/runner-docs-maintainers'].freeze

    react_to 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        !merge_request_labelled_technical_writing? &&
        workflow_ready_for_review_added? &&
        merge_request_changes_doc? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_documentation_label_comment
    end

    def documentation
      <<~TEXT
        This processor identifies merge requests with documentation changes,
        labels them with ~"Technical Writing" and pings a technical writer to review.
      TEXT
    end

    private

    def merge_request_changes_doc?
      Triage::ChangesChecker.new(project_id, merge_request_iid).any_change?(DOC_FILE_REGEX)
    end

    def merge_request_labelled_technical_writing?
      event.label_names.any? do |label|
        label == TECHNICAL_WRITING_LABEL ||
        label.start_with?('tw::')
      end
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def post_documentation_label_comment
      comment = <<~MARKDOWN.chomp
        #{message if approvers.any?}
        /label ~"#{DOCUMENTATION_LABEL}" ~"#{TECHNICAL_WRITING_TRIAGED_LABEL}"
      MARKDOWN

      add_comment(comment.strip, append_source_link: approvers.any?)
    end

    def message
      comment = <<~MESSAGE
        Hi #{approver_usernames}! Please review this ~"#{DOCUMENTATION_LABEL}" merge request.
      MESSAGE

      if approvers.include?(GL_DOCSTEAM_HANDLE)
        comment += <<~MESSAGE

          Please also consider updating the `CODEOWNERS` file in the #{event.project_web_url} project.
        MESSAGE
      end

      if reviewers.any?
        comment += <<~ASSIGN_REVIEWERS.chomp
          /assign_reviewer #{reviewer_usernames}
        ASSIGN_REVIEWERS
      end

      unique_comment.wrap(comment)
    end

    def approvers
      @approvers ||=
        Triage::DocumentationCodeOwner
          .new(project_id, merge_request_iid)
          .approvers
    end

    def approver_usernames
      @approver_usernames ||=
        approvers.map do |username|
          technical_writer_group?(username) ? "@#{username}" : "`@#{username}`"
        end.join(' ')
    end

    def technical_writer_group?(username)
      TECHNICAL_WRITER_GROUPS.include?(username)
    end

    def reviewers
      @reviewers ||= approvers.reject { |username| technical_writer_group?(username) }
    end

    def reviewer_usernames
      @reviewer_usernames ||= reviewers.map { |username| "@#{username}" }.join(' ')
    end
  end
end
