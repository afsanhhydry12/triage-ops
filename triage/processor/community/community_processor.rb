# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  class CommunityProcessor < Processor
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
    WORKFLOW_IN_DEV_LABEL = 'workflow::in dev'
    WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

    def wider_community_contribution?
      event.label_names.include?(COMMUNITY_CONTRIBUTION_LABEL)
    end

    def wider_community_contribution_open_resource?
      event.resource_open? &&
        wider_community_contribution?
    end

    def valid_command?
      command.valid?(event) &&
        (event.by_resource_author? || event.by_team_member?)
    end

    def workflow_in_dev_added?
      event.added_label_names.include?(WORKFLOW_IN_DEV_LABEL)
    end

    def workflow_ready_for_review_added?
      event.added_label_names.include?(WORKFLOW_READY_FOR_REVIEW_LABEL)
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
