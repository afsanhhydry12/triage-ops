# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changes_checker'

module Triage
  class ProdOpsFlowNotifier < Processor
    PROD_OPS_PATHS = [
      'sites/handbook/source/handbook/product-development-flow/',
      'sites/handbook/source/handbook/product/product-principles/',
      'sites/handbook/source/handbook/marketing/blog/release-posts/',
      'sites/handbook/source/handbook/product/product-processes/',
      '.gitlab/issue_templates/release-post-mvp-nominations.md',
      '.gitlab/issue_templates/Release-Post-Retrospective.md',
      '.gitlab/issue_templates/product-development-retro.md',
      '.gitlab/merge_request_templates/Change-Product-Handbook.md',
      '.gitlab/merge_request_templates/Release-Post-Bug-Performance-Usability-Improvement-Block.md',
      '.gitlab/merge_request_templates/Release-Post-Item.md',
      '.gitlab/merge_request_templates/Release-Post.md',
      'source/includes/_performance_'
    ].freeze

    # Note: we can only associate labels to files that are in PROD_OPS_PATHS
    LABELS_FOR_FILE = {
      'sites/handbook/source/handbook/product-development-flow/' =>
        ['Product Operations', 'prodops:release', 'product development flow', 'product handbook'].freeze,
      'sites/handbook/source/handbook/product/product-principles/' =>
        ['Product Operations', 'prodops:release', 'product development flow', 'product handbook'].freeze,
      'sites/handbook/source/handbook/marketing/blog/release-posts/' =>
        ['Product Operations', 'prodops:release', 'release post'].freeze,
      'sites/handbook/source/handbook/product/product-processes/' =>
        ['Product Operations', 'prodops:release', 'product development flow', 'product handbook'].freeze,
      '.gitlab/issue_templates/release-post-mvp-nominations.md' =>
        ['Product Operations', 'prodops:release', 'release post', 'templates'].freeze,
      '.gitlab/issue_templates/Release-Post-Retrospective.md' =>
        ['Product Operations', 'prodops:release', 'release post', 'templates'].freeze,
      '.gitlab/issue_templates/product-development-retro.md' =>
        ['Product Operations', 'prodops:release', 'product development flow', 'templates'].freeze,
      '.gitlab/merge_request_templates/Change-Product-Handbook.md' =>
        ['Product Operations', 'prodops:release', 'product handbook', 'templates'].freeze,
      '.gitlab/merge_request_templates/Release-Post-Bug-Performance-Usability-Improvement-Block.md' =>
        ['Product Operations', 'prodops:release', 'release post', 'templates'].freeze,
      '.gitlab/merge_request_templates/Release-Post-Item.md' =>
        ['Product Operations', 'prodops:release', 'release post', 'templates'].freeze,
      '.gitlab/merge_request_templates/Release-Post.md' =>
        ['Product Operations', 'prodops:release', 'release post', 'templates'].freeze,
      'source/includes/_performance_' =>
        ['Product Operations', 'prodops:release', 'product handbook'].freeze
    }.freeze

    react_to 'merge_request.update', 'merge_request.open'

    def applicable?
      event.from_www_gitlab_com? &&
        prod_ops_related_change? &&
        unique_comment.no_previous_comment?
    end

    def process
      add_comment(review_request_comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor requests DRIs to review merge request when changes are done to Product Operations related pages.
      TEXT
    end

    private

    def prod_ops_related_change?
      changes_checker.any_change?(PROD_OPS_PATHS)
    end

    def changes_checker
      @changes_checker ||= Triage::ChangesChecker.new(event.project_id, event.iid)
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def review_request_comment
      comment = <<~MARKDOWN.chomp
        @brhea @fseifoddini please review this Product Operations related Merge Request.

        #{labels}
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def labels
      labels = changes_checker.merge_request_changes.flat_map do |change|
        labels_for_change(change)
      end.uniq

      return '' if labels.empty?

      "/label #{labels.map { |label| "~\"#{label}\"" }.join(' ')}"
    end

    def labels_for_change(change)
      Triage::ChangesChecker::OLD_NEW_PATHS.flat_map do |old_or_new|
        path = change[old_or_new]
        LABELS_FOR_FILE[path] || []
      end
    end
  end
end
