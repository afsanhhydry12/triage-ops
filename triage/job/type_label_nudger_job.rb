# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/markdown_table'
require_relative '../triage/result'
require_relative '../triage/unique_comment'
require_relative '../processor/type_label_nudger'

module Triage
  class TypeLabelNudgerJob < Job
    include Reaction

    TYPE_LABEL_MISSING_MESSAGE = 'Please add ~"type::bug" ~"type::feature", ~"type::maintenance" or a [subtype](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) label to this merge request.'

    private

    def execute(event)
      prepare_executing_with(event)
      return unless applicable?

      result = validate

      add_discussion(type_label_nudger_comment(result), append_source_link: false)
    end

    def applicable?
      resource_open? && !community_contribution? && need_to_nudge?
    end

    def resource_open?
      merge_request.state == 'opened'
    end

    def community_contribution?
      merge_request.labels.include?(Triage::TypeLabelNudger::COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      !type_label_present? && !unique_comment.previous_discussion
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def type_label_present?
      merge_request.labels.any? { |label| label.start_with?('type::', 'bug::', 'feature::', 'maintenance::') }
    end

    def type_label_nudger_comment(validated_result)
      table = MarkdownTable.new(validated_result).markdown

      comment = <<~MARKDOWN.chomp
        :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
        #{table}
      MARKDOWN
      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new('Triage::TypeLabelNudger', event)
    end

    def validate
      result = Triage::Result.new

      result.errors << TYPE_LABEL_MISSING_MESSAGE unless type_label_present?

      result
    end
  end
end
