# frozen_string_literal: true

module Triage
  module Strings
    module Thanks
      ready_for_review = %(When you're ready for a first review, post `@gitlab-bot ready`. If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.)
      request_help = 'At any time, if you need help moving the MR forward, feel free to post `@gitlab-bot help`. Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).'
      danger_setup = "To enable automated checks on your MR, please [configure Danger for your fork](https://docs.gitlab.com/ee/development/dangerbot.html#configuring-danger-for-forks)."
      group_label = 'You can comment `@gitlab-bot label <label1> <label2>` to add labels to your MR. Please see the list of allowed labels in the [`label` command documentation](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#reactive-label-command).'

      runner_body = <<~MARKDOWN.chomp
        Some contributions require several iterations of review and we try to mentor contributors
        during this process. However, we understand that some reviews can be very time consuming.
        If you would prefer for us to continue the work you've submitted now or at any point in the
        future please let us know.

        If you're okay with being part of our review process (and we hope you are!), there are
        several initial checks we ask you to make:

        * The merge request description clearly explains:
          * The problem being solved.
          * The best way a reviewer can test your changes (is it possible to provide an example?).
        * If the pipeline failed, do you need help identifying what failed?
        * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
        * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
        document.
      MARKDOWN

      gitlab_foss_body = <<~MARKDOWN.chomp
        Hey @%<author_username>s! :wave:

        Thank you for your contribution. GitLab has moved to a single codebase for GitLab CE and GitLab EE.

        Please do not create merge requests here. Instead, create them at https://gitlab.com/gitlab-org/gitlab/-/merge_requests.

        /close
      MARKDOWN

      www_gitlab_com_body = <<~MARKDOWN.chomp
        I'll notify the Website team about your Merge Request and they will get back to you as soon
        as they can.
        If you don't hear from someone in a reasonable amount of time, please ping us again in a
        comment and mention @gitlab-com-community.
      MARKDOWN

      intro_thanks = <<~MARKDOWN.chomp
        Hey @%<author_username>s! :wave:

        Thank you for your contribution to GitLab. Please refer to the [contribution flow documentation](https://docs.gitlab.com/ee/development/contributing/#contribution-flow) for a quick overview of the process, and the [merge request (MR) guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) for the detailed process.
      MARKDOWN

      signoff_thanks = <<~MARKDOWN.chomp
        /label ~"Community contribution" ~"workflow::in dev"
        /assign @%<author_username>s
      MARKDOWN

      DEFAULT_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{ready_for_review}

        #{request_help}

        #{danger_setup}

        #{group_label}

        #{signoff_thanks}
      MARKDOWN

      GITLAB_FOSS_THANKS = <<~MARKDOWN.chomp
        #{gitlab_foss_body}
        #{signoff_thanks}
      MARKDOWN

      RUNNER_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{ready_for_review}

        #{request_help}

        #{runner_body}

        #{signoff_thanks}
      MARKDOWN

      WWW_GITLAB_COM_THANKS = <<~MARKDOWN.chomp
        #{intro_thanks}

        #{www_gitlab_com_body}

        #{signoff_thanks}
      MARKDOWN
    end
  end
end
