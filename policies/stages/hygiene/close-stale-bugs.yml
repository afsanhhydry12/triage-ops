resource_rules:
  issues:
    rules:
      - name: Remove stale label from active vintage bug
        conditions: # Should have been human updated in the last 7 days and this should be prioritized
          state: opened
          labels:
            - type::bug
            - vintage
            - stale
          ruby: |
            StaleResources.active?(project_path: ENV['TRIAGE_SOURCE_PATH'], token: ENV['GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN'], project_id: resource[:project_id], resource_iid: resource[:iid], days: 7)
        actions:
          remove_labels: stale
      - name: Identify eligible vintage bugs that can be closed and auto-close them
        conditions: # Should not have been human updated in the last 7 days.
          state: opened
          labels:
            - type::bug
            - vintage
            - stale
          date:
            attribute: updated_at
            condition: older_than
            interval_type: days
            interval: 6
          ruby: |
            StaleResources.stale?(project_path: ENV['TRIAGE_SOURCE_PATH'], token: ENV['GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN'], project_id: resource[:project_id], resource_iid: resource[:iid], days: 372)
        actions:
          status: close
          labels:
            - auto closed
          comment: |
            Hi {{author}} :wave:,

            Based on [the policy for inactive bugs](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#auto-close-inactive-bugs), this issue is now being closed.

            If you think this bug still exists, and is reproducible with [the latest stable version of GitLab](https://docs.gitlab.com/ee/policy/maintenance.html#gitlab-release-and-maintenance-policy), please reopen this issue.

            Thanks for your contributions to make GitLab better!
      - name: Identify vintage bugs
        conditions:
          state: opened
          date:
            attribute: created_at
            condition: older_than
            interval_type: days
            interval: 365
          labels:
            - type::bug
          forbidden_labels:
            - vintage # If already a vintage issue, it means that this exercise has happened already. Leave it as such.
          ruby: |
            StaleResources.stale?(project_path: ENV['TRIAGE_SOURCE_PATH'], token: ENV['GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN'], project_id: resource[:project_id], resource_iid: resource[:iid], days: 365)
        actions:
          labels:
            - vintage
      - name: Identify stale bugs and add warning on auto-closure
        conditions:
          state: opened
          labels:
            - type::bug
            - vintage
          forbidden_labels:
            - severity::1
            - severity::2
            - customer
            - customer+
            - security
            - stale
            - automation:prevent-auto-close
          upvotes:
            attribute: upvotes
            condition: less_than
            threshold: 11
          limits:
            oldest: 100
          ruby: |
            StaleResources.stale?(project_path: ENV['TRIAGE_SOURCE_PATH'], token: ENV['GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN'], project_id: resource[:project_id], resource_iid: resource[:iid], days: 365)
        actions:
          comment: |
            Hi {{author}} :wave:,

            Thanks for raising this bug.

            Contributions like this are vital to help make GitLab a better product.

            We would be grateful for your help in verifying whether your bug report requires further attention from the team. If you think this bug still exists, and is reproducible with [the latest stable version of GitLab](https://docs.gitlab.com/ee/policy/maintenance.html#gitlab-release-and-maintenance-policy), please comment on this issue.

            This issue has been inactive for more than 12 months now and based on [the policy for inactive bugs](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#auto-close-inactive-bugs), will be closed in 7 days.

            Thanks for your contributions to make GitLab better!
          labels:
            - stale
