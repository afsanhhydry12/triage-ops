.common_actions_remove_weight: &common_actions_remove_weight
  comment: |
    The weight has been removed from this issue, according to the [Protect and Threat Insights](https://about.gitlab.com/handbook/engineering/development/sec/protect/)
    refinement process for [bugs](https://about.gitlab.com/handbook/engineering/development/sec/protect/planning/#bug-diagnosis)
    and [spikes](https://about.gitlab.com/handbook/engineering/development/sec/protect/planning/#refinement-for-spikes).

    If you want to indicate the complexity of a ~"type::bug" or ~"spike", please
    use the `backend-weight` and/or the `frontend-weight` labels instead.

    [Bot policy](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/policies/groups/gitlab-org/threat-insights/issue-weights.yml).

    /clear_weight

resource_rules:
  issues:
    rules:
      - name: Remove weights from bugs and issues
        conditions:
          state: opened
          weight: Any
          labels:
            - "group::threat insights"
            - "{type::bug,spike}"
        actions:
          <<: *common_actions_remove_weight
      - name: Put engineering issues missing weight back into refinement
        conditions:
          state: opened
          weight: None
          forbidden_labels:
            - type::bug
            - spike
          labels:
            - "group::threat insights"
            - "{backend,frontend}"
            - "workflow::ready for development"
        actions:
          comment: |
            This issue has been sent back to ~"workflow::refinement" because it's missing the weight. Please add a
            weight and then send it back to ~"workflow::ready for development". If indeed this issue does not need a
            weight, then it must be either (labelled as a ~spike or ~bug), or (unlabelled as ~frontend and ~backend).

            [Bot policy](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/policies/groups/gitlab-org/threat-insights/issue-weights.yml).
          labels:
            - workflow::refinement
